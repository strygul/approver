package com.iev.models.approver.customfield

import com.atlassian.jira.issue.customfields.impl.GenericTextCFType
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport
import javax.inject.Inject

@Scanned
class GridFieldType @Inject()(@JiraImport customFieldValuePersister: CustomFieldValuePersister,
                              @JiraImport genericConfigManager: GenericConfigManager,
                              @JiraImport textFieldCharacterLengthValidator: TextFieldCharacterLengthValidator,
                              @JiraImport jiraAuthenticationContext: JiraAuthenticationContext)
  extends GenericTextCFType(
    customFieldValuePersister,
    genericConfigManager,
    textFieldCharacterLengthValidator,
    jiraAuthenticationContext) {

}
